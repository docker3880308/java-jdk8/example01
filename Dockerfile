FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 8080
COPY target/demo-isoft-0.0.1-SNAPSHOT.jar demo-isoft.jar
ENTRYPOINT ["java","-jar","demo-isoft.jar"]