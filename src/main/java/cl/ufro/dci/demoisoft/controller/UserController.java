package cl.ufro.dci.demoisoft.controller;

import cl.ufro.dci.demoisoft.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    private List<User> listaUsuarios = new ArrayList<>();


    @GetMapping("/usuarios")
    public List<User> crearUsuarios(){
        this.listaUsuarios.add(new User("jona","jona06"));
        this.listaUsuarios.add(new User("maca","maca08"));
        return this.listaUsuarios;
    }


}
