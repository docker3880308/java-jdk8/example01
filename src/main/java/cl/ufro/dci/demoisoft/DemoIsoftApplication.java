package cl.ufro.dci.demoisoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoIsoftApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoIsoftApplication.class, args);

		System.out.println("hola mundo");
	}

}
