package cl.ufro.dci.demoisoft.model;

public class User {
    public String name;
    public String userName;

    public User(String name, String userName) {
        this.name = name;
        this.userName = userName;
    }
}
