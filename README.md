# Proyecto de ejemplo para un contenedor en Docker


## Contexto:

El proyecto consiste básicamente en conectar un backend ultra sencillo, realizado en Java 8 con un archivo Dockerfile que se encarga de crear la imagen y posteriormente ejecutar el contenedor.

## Herramientas:

    * Java jdk-8
    * Maven
    * Docker
    * IntelliJ IDEA
    * Git
